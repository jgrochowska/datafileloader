import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataLoaderUtils {

    public void readFile(String path, int batchSize) {
        DatabaseConnection databaseConnection = new DatabaseConnection();
        Connection connection = null;
        try {
            connection = databaseConnection.initializeDatabase();

            databaseConnection.createDatabase(connection);

            String fileExtension = path.substring(path.lastIndexOf('.') + 1).toLowerCase();

            if (fileExtension.equals("csv") || fileExtension.equals("txt")) {
                readCSV(path, connection, batchSize);
                System.out.println("Upload file csv");
            } else if (fileExtension.equals("xml")) {
                readXML(path, connection, batchSize);
                System.out.println("Upload file xml");
            } else {
                System.out.println(fileExtension + ": this file format is not supported");
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readCSV(String path, Connection connection, int batchSize) {
        try {
            connection.setAutoCommit(false);

            PreparedStatement customersStatement = connection.prepareStatement(Constants.SQL_CUSTOMERS);
            PreparedStatement contactsStatement = connection.prepareStatement(Constants.SQL_CONTACTS);

            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));

            int count = 0;
            int id = findNextCustomerId(connection);
            String line = null;

            while ((line = bufferedReader.readLine()) != null) {
                String[] array = line.split(",");
                String name = array[0];
                String surname = array[1];
                String age = array[2];

                setCustomerStatement(customersStatement, id, name, surname, age);

                String contact = "";
                for (int i = 4; i < array.length; i++) {
                    contact = array[i];

                    contactsStatement.setInt(1, id);
                    contactsStatement.setString(2, contact);

                    if (validateEmail(contact)) {
                        contactsStatement.setInt(3, 1);
                        contactsStatement.setInt(3, ContactType.EMAIL.getType());
                    } else if (validatePhone(contact)) {
                        contactsStatement.setInt(3, ContactType.PHONE.getType());
                    } else if (validateJabber(contact)) {
                        contactsStatement.setInt(3, ContactType.JABBER.getType());
                    } else {
                        contactsStatement.setInt(3, ContactType.UNKNOWN.getType());
                    }

                    contactsStatement.addBatch();
                }
                id++;
                customersStatement.addBatch();
                if (++count % batchSize == 0) {
                    customersStatement.executeBatch();
                }

                if (++count % batchSize == 0) {
                    contactsStatement.executeBatch();
                }
            }

            bufferedReader.close();
            customersStatement.executeBatch();
            contactsStatement.executeBatch();
            connection.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readXML(String path, Connection connection, int batchSize) {

        try {
            connection.setAutoCommit(false);

            PreparedStatement customersStatement = connection.prepareStatement(Constants.SQL_CUSTOMERS);
            PreparedStatement contactsStatement = connection.prepareStatement(Constants.SQL_CONTACTS);

            int count = 0;
            int id = findNextCustomerId(connection);

            Document document = getDocument(path);
            NodeList personList = document.getElementsByTagName("person");
            for (int i = 0; i < personList.getLength(); i++) {
                Node node = personList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    String name = getNode("name", element);
                    String surname = getNode("surname", element);
                    String age = getNode("age", element);

                    setCustomerStatement(customersStatement, id, name, surname, age);

                    NodeList contactList = element.getElementsByTagName("contact");

                    for (int j = 0; j < contactList.getLength(); j++) {
                        Node nodeContact = contactList.item(j);

                        if (nodeContact.getNodeType() == nodeContact.ELEMENT_NODE) {
                            Element contact = (Element) nodeContact;

                            contactsStatement.setInt(1, id);
                            contactsStatement.setString(2, contact.getTextContent());

                            if (contact.getAttribute("type").equals("email")) {
                                contactsStatement.setInt(3, ContactType.EMAIL.getType());
                            } else if (contact.getAttribute("type").equals("phone")) {
                                contactsStatement.setInt(3, ContactType.PHONE.getType());
                            } else if (contact.getAttribute("type").equals("jabber")) {
                                contactsStatement.setInt(3, ContactType.JABBER.getType());
                            } else {
                                contactsStatement.setInt(3, ContactType.UNKNOWN.getType());
                            }

                        }
                        contactsStatement.addBatch();
                    }
                    id++;
                }
                customersStatement.addBatch();
                if (++count % batchSize == 0) {
                    customersStatement.executeBatch();
                }

                if (++count % batchSize == 0) {
                    contactsStatement.executeBatch();
                }
            }
            customersStatement.executeBatch();
            contactsStatement.executeBatch();
            connection.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setCustomerStatement(PreparedStatement customersStatement, int id, String name, String surname, String age) throws SQLException {
        customersStatement.setInt(1, id);
        customersStatement.setString(2, name);
        customersStatement.setString(3, surname);

        if (!age.equals("")) {
            Integer ageInt = Integer.parseInt(age);
            customersStatement.setInt(4, ageInt);
        } else {
            customersStatement.setString(4, null);
        }
    }

    private Document getDocument(String path) throws ParserConfigurationException, SAXException, IOException {
        File xmlFile = new File(path);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document document = dBuilder.parse(xmlFile);
        document.getDocumentElement().normalize();
        return document;
    }

    private String getNode(String tag, Element element) {
        if (element.getElementsByTagName(tag).item(0) != null) {
            NodeList list = element.getElementsByTagName(tag).item(0).getChildNodes();
            if ((list.getLength() == 0))
                return "";
            Node value = (Node) list.item(0);
            return value.getNodeValue();
        }
        return "";
    }

    private int findNextCustomerId(Connection connection) {
        int id = 0;
        String sql = "SELECT Id from customers order by Id desc limit 1";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                id = resultSet.getInt(1);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id + 1;
    }

    private boolean validateEmail(String contact) {
        String emailRegex = "^(.+)@(.+)$";
        Pattern patternEmail = Pattern.compile(emailRegex);
        Matcher matcherEmail = patternEmail.matcher(contact);

        return matcherEmail.matches();
    }

    private boolean validatePhone(String contact) {
        String phoneRegex = "^\\d{9}|(?:\\d{3} ){2}\\d{3}$";
        Pattern patternPhone = Pattern.compile(phoneRegex);
        Matcher matcherPhone = patternPhone.matcher(contact);

        return matcherPhone.matches();
    }

    private boolean validateJabber(String contact) {
        String jabberRegex = "jbr";
        Pattern patternJabber = Pattern.compile(jabberRegex);
        Matcher matcherJabber = patternJabber.matcher(contact);

        return matcherJabber.matches();
    }
}
