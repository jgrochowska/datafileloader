import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DatabaseConnection {

    public Connection initializeDatabase() throws SQLException, ClassNotFoundException, IOException {
        String url = "jdbc:mysql://localhost:3306/customers_db?serverTimezone=UTC&useSSL=false";
        String dbDriver = "com.mysql.cj.jdbc.Driver";
        String user = "root";
        String pass = "fotografia1";

//        Properties properties = new Properties();
//        properties.load(ClassLoader.getSystemResourceAsStream("app.properties"));
//        connection = DriverManager.getConnection(url, properties);

        Class.forName(dbDriver);
        Connection connection = DriverManager.getConnection(url, user, pass);

        return connection;
    }

    public void createDatabase(Connection connection) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            // statement.executeUpdate(Constants.SQL_DROP_TABLES);
            statement.executeUpdate(Constants.SQL_CUSTOMERS_CREATE);
            statement.executeUpdate(Constants.SQL_CONTACTS_CREATE);

            System.out.println("Tables created");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
