import java.sql.Connection;
import java.sql.SQLException;

public class Main {


    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/customers_db?serverTimezone=UTC&useSSL=false";
        String pathCSV = "dane-osoby.txt";
        String pathXML = "dane-osoby.xml";

        DataLoaderUtils utils = new DataLoaderUtils();
        utils.readFile(pathCSV, 1000);
        utils.readFile(pathXML, 1000);

    }
}
