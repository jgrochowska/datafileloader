public class Constants {

    public static final String SQL_CUSTOMERS_CREATE = "CREATE TABLE Customers (\n" +
            "Id BIGINT PRIMARY KEY NOT NULL,\n" +
            "Name VARCHAR(255) NOT NULL,\n" +
            "Surname VARCHAR(255) NOT NULL,\n" +
            "Age INT\n"
            + ")";

    public static final String SQL_CONTACTS_CREATE = "CREATE TABLE Contacts (\n" +
            "Id BIGINT PRIMARY KEY auto_increment,\n" +
            "Id_customer BIGINT,\n" +
            "Contact VARCHAR(255),\n" +
            "Type INT,\n" +
            "FOREIGN KEY(Id_customer) REFERENCES Customers(Id) ON DELETE CASCADE\n" +
            ")";

    public static final String SQL_DROP_TABLES = "DROP TABLE IF EXISTS Customers, Contacts";
    public static final String SQL_CUSTOMERS = "INSERT INTO customers (ID, NAME, SURNAME, AGE) VALUES (?,?, ?, ?) ";
    public static final String SQL_CONTACTS = "INSERT INTO contacts (ID_CUSTOMER, CONTACT, TYPE) VALUES (?,?,?) ";

    public static final String UPLOAD_DIRECTORY = "upload";

    public static final int MEMORY_THRESHOLD = 1024 * 1024 * 3;
    public static final int MAX_FILE_SIZE = 1024 * 1024 * 40;
    public static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50;

}
