import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;


@WebServlet(name = "SaveServlet",
        urlPatterns = {"/saveFile"}
)
public class SaveServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uploadPath = getServletContext().getRealPath("") + File.separator + Constants.UPLOAD_DIRECTORY;
        String filePath = fileUploadedPath(uploadPath);
        DataLoaderUtils utils = new DataLoaderUtils();

        try {
            utils.readFile(filePath, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);

    }

    private String fileUploadedPath(String uploadPath) {
        File directory = new File(uploadPath);
        File[] listOfFiles = directory.listFiles();
        String path = "";
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                path = listOfFiles[i].getAbsolutePath();
            } else {
                System.out.println("Directory is null");
            }
        }
        return path;
    }


}



